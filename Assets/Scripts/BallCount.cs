﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BallCount : MonoBehaviour {

    public Text BallCounter;
    public int CountBall = 5;
	void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {
        // ball count reduce by 1 per shot
        if (Input.GetKeyDown(KeyCode.Space))
        {
            CountBall--;
            BallCounter.text = "" + CountBall;
        }
        //if below zero then game over
        if (CountBall < 0)
        {
            SceneManager.LoadScene("GameOver");
        }

    }
}
