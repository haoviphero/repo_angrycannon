﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadMap : MonoBehaviour {

    public void ArticMap()
    {
        SceneManager.LoadScene("Artic");
    }
    public void DesertMap()
    {
        SceneManager.LoadScene("Desert");
    }

}
