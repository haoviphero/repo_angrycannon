﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonFire : MonoBehaviour
{

    public GameObject CannonBall;
    public Transform spawnerPos;
    public float launchVelocity = 20;
    Vector3 iniitialPos;
    GameObject clone;

    
    //Cannon ball will shoot aligned to the cannon's aim
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {

            GameObject clone = Instantiate(CannonBall, spawnerPos.position, spawnerPos.rotation);
            clone.GetComponent<Rigidbody2D>().velocity = spawnerPos.up * launchVelocity;
            Destroy(clone, 5);
        }
    }
    
   

   
}



















