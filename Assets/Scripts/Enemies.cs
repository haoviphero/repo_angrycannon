﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Enemies : MonoBehaviour {
    public float AIhp = 4f;
    public GameObject deathEffect;
    public static int EnemiesRemains = 0;
    //Enemies counter (win condition)
    void Start()
    {
        EnemiesRemains++;   
    }
    // Enemies hp settings
    void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.relativeVelocity.magnitude > AIhp)
        {
            Explode();
        }
       // Debug.Log(collision.relativeVelocity.magnitude);
    }
    // enemies death effect and enemies death to win condition
    void Explode()
    {
        Instantiate(deathEffect, transform.position, Quaternion.identity);
        Destroy(gameObject);

        EnemiesRemains--;
        if(EnemiesRemains <= 0)
        {
            SceneManager.LoadScene("WIN");
        }
    }
}
