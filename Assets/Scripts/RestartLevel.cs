﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartLevel : MonoBehaviour {


    public void Restart()
    {
        SceneManager.LoadScene("MapSelect");
    }

    public void ReturntoMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

}
