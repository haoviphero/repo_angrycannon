﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class NewCountDown : MonoBehaviour {
    public Text Counter;
    public int CountTime = 60;

	// call for a time update every 1 sec
	void Start () {
        InvokeRepeating("Timer",1,1);
	}
    private void Timer()
    {
        CountTime--;
        Counter.text = ""+CountTime; 
        //load lose screen if time runs out
        if(CountTime == 0)
        {
            SceneManager.LoadScene("GameOver");
        }
    }
    

}
