﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCount : MonoBehaviour {

    List<GameObject> listOfEnemy = new List<GameObject>();
    public float AIhp = 4f;


    void Start()
    {
        listOfEnemy.AddRange(GameObject.FindGameObjectsWithTag("Enemy"));
        print(listOfEnemy.Count);


    }
    
    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.relativeVelocity.magnitude > AIhp)
        {
            Explode();
        }
        // Debug.Log(collision.relativeVelocity.magnitude);
    }

    void Explode()
    {
        Destroy(gameObject);
    }



    public void KilledOpponent(GameObject enemy)
    {
        if (listOfEnemy.Contains(enemy))
        {
            listOfEnemy.Remove(enemy);
            Debug.Log("One enemy removed");
        }

        print(listOfEnemy.Count);

    }



    public bool AreOpponentsDead()
    {
        if (listOfEnemy.Count <= 0)
        {
            // They are dead!
            Debug.Log("Game win");
            return true;
        }
        else
        {
            // They're still alive dangit
            return false;
            Debug.Log("Game over");
        }
    }
}
